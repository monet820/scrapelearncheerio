const packageNeedle = require("needle");
const packageCheerio = require("cheerio");
const fs = require("fs");


packageNeedle.get("https://en.wikipedia.org/wiki/Main_Page", {
    json: true
}, (error, response) => {
    if (error) {
        console.log(error);
    }

    // Get body from response.
    let body = response.body;

    const $ = packageCheerio.load(body);

    // Get Todays article, image.
    let imgSrc = $("#mp-tfa-img").find('img').attr('src');

    console.log(imgSrc);


    // Get todays article heading.
    let newName = $('span[id="From_today\'s_featured_article"]').html();
    newName = newName.replace("&apos;", '\'');

    console.log("\n" + newName);

    // Get todays article, text
    let articleText = $('#mp-tfa-img').next().text();
    console.log("\n" + articleText);


    const returnedGoods = {
        imageSource: imgSrc,
        header: newName,
        paragraph: articleText
    }

    // Create new file named stolenGoods.json and write the strings to it.
    fs.writeFile("./stolenGoods.json", JSON.stringify(returnedGoods),
        (error) => {
            if (error) {
                console.log("Callback, was called. Could not write to file.");
            }
        }
    )

});